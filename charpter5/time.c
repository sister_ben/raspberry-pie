/*************************************************************************
    > File Name: time.c
    > Author: Lee_Yellow
    > Mail: 1031208128@qq.com 
    > Created Time: Wed Nov  3 22:03:57 2021
 ************************************************************************/
#include <stdio.h>
#include <time.h>

time_t start, end;

int main(){

	int i;
	start = time(NULL);
	printf("start=%ld\n", start);
	for(i = 0; i<123456789; i++);
	end = time(NULL);
	printf("end = %ld time=%ld\n", end, end-start);
	
}
