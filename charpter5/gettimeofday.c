/*************************************************************************
    > File Name: gettimeofday.c
    > Author: Lee_Yellow
    > Mail: 1031208128@qq.com 
    > Created Time: Wed Nov  3 21:35:05 2021
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

struct timeval t;
int main(){

	gettimeofday(&t, NULL);
	printf("sec=%ld usec=%d\n", t.tv_sec, t.tv_usec);
	printf((char *)ctime(&t.tv_sec));
}
