/*************************************************************************
    > File Name: gettimeofday.c
    > Author: Lee_Yellow
    > Mail: 1031208128@qq.com 
    > Created Time: Wed Nov  3 21:35:05 2021
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

struct timeval t;
int main(){
	int r;
	t.tv_sec = 123456789;
	t.tv_usec = 0;
	r = settimeofday(&t, NULL);
	if(r){
		printf("settimeofday() failed\n");
		exit(1);
	}
	gettimeofday(&t, NULL);
	printf("sec=%ld usec=%d\n", t.tv_sec, t.tv_usec);
	printf((char *)ctime(&t.tv_sec));
}
