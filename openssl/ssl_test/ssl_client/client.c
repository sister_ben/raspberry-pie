/*************************************************************************
    > File Name: client.c
    > Author: Lee_Yellow
    > Mail: 1031208128@qq.com 
    > Created Time: Mon Nov 29 10:36:03 2021
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define CHK_NULL(x) if ((x)==NULL) exit (1)
#define CHK_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(2); }


void main ()
{
	int err;
	int sd;					//socket 句柄
	struct sockaddr_in sa;	//sockaddr_in结构体
	SSL_CTX* ctx;			//SSL上下文句柄
	SSL*     ssl;			//SSL结构体指针
	X509*    server_cert;	//X509结构体，用户保存服务器端证书	
	char*    str;
	char     buf [4096];
	SSL_METHOD *meth;		//SSL协议

	//初始化OpenSSL环境
	SSL_load_error_strings();
	SSLeay_add_ssl_algorithms();
	//SSL协议版本，V2、V3自适应
	meth = TLSv1_client_method();
	ctx = SSL_CTX_new (meth);  
	CHK_NULL(ctx);
	CHK_SSL(err);
	

	//以常规的SOCKET编程的方式创建socket并链接到服务器端
	sd = socket (AF_INET, SOCK_STREAM, 0);
	CHK_ERR(sd, "socket");
	memset (&sa, '\0', sizeof(sa));
	sa.sin_family      = AF_INET;
	sa.sin_addr.s_addr = inet_addr ("127.0.0.1");   //服务端地址
	sa.sin_port        = htons     (8443);          //服务端口
	//链接服务器
	err = connect(sd, (struct sockaddr*) &sa,
		sizeof(sa));                
	CHK_ERR(err, "connect");
	
	//使用现有的TCP链接开启SSL协议
	ssl = SSL_new (ctx);       
	CHK_NULL(ssl);    
	SSL_set_fd (ssl, sd);
	//启动SSL链接
	err = SSL_connect (ssl);
	CHK_SSL(err);
	//打印SSL链接的算法
	printf ("SSL connection using %s\n", SSL_get_cipher (ssl));
	
	//获得服务端证书	
	server_cert = SSL_get_peer_certificate (ssl);       CHK_NULL(server_cert);
	printf ("Server certificate:\n");
	//获得服务器证书名称
	str = X509_NAME_oneline (X509_get_subject_name (server_cert),0,0);
	CHK_NULL(str);
	printf ("\t subject: %s\n", str);
	OPENSSL_free (str);
	//获得服务器证书颁发者名称	
	str = X509_NAME_oneline (X509_get_issuer_name  (server_cert),0,0);
	CHK_NULL(str);
	printf ("\t issuer: %s\n", str);
	OPENSSL_free (str);
	//释放X509结构体
	X509_free (server_cert);
	
	//发送消息到服务端
	err = SSL_write (ssl, "Hello World!", strlen("Hello World!")); 
	CHK_SSL(err);
	//读取服务端的消息
	err = SSL_read (ssl, buf, sizeof(buf) - 1);      
	CHK_SSL(err);
	buf[err] = '\0';
	printf ("Got %d chars:'%s'\n", err, buf);
	SSL_shutdown (ssl);  //发送SSL关闭消息
	
	//释放内存
	
	SSL_free (ssl);
	SSL_CTX_free (ctx);
}
