/*************************************************************************
    > File Name: sm3.c
    > Author: Lee_Yellow
    > Mail: 1031208128@qq.com 
    > Created Time: Thu Nov 11 14:11:19 2021
 ************************************************************************/
//KDF函数的实现基于GB_T32918_4-2016 5.3.4章《密钥派生函数》实现
//函数功能：KDF秘钥派生函数
//输入参数： Z     -用于计算的比特串
//			Zlen   -Z的字节数
//          klen   -需要获得秘钥数据的字节长度
//输出参数： K      -长度为klen的秘钥数据比特串K（这里设置为128位）
#include <stdio.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <stdlib.h>
#include <string.h>

void my_KDF(const unsigned char* Z, const unsigned int Zlen, const unsigned int Klen, unsigned char* K)
{
	unsigned int ct = 1;					//计数器
	unsigned char cCt[4] = { 0 };           //计数器的内存表示值

	unsigned char *pData = new unsigned char[Zlen + 4];
	unsigned char cdgst[32] = { 0 };        //摘要
	int nDgst = 32;						    //摘要长度 单位：字节
	unsigned int hash_len;					//sm3输出串长度  
	int index = (Klen + 32) / 32;			//需要计算的次数，这里+32表示至少要进行一次循环

	if (Z == NULL || Klen <= 0 || Zlen < 0 || K == NULL)
	{
		return -1;
	}
	//初始化空间
	memset(pData, 0, Zlen + 4);
	//复制比特串Z
	memcpy(pData, Z, Zlen);

	for (size_t i = 0; i < index ; i++)
	{
		//内存表示计数器
		{
			cCt[0] = (ct >> 24) & 0xFF;
			cCt[1] = (ct >> 16) & 0xFF;
			cCt[2] = (ct >> 8) & 0xFF;
			cCt[3] = (ct) & 0xFF;
		}
		//拼接比特串
		memcpy(pData + Zlen, cCt, 4);
		//sm3函数处理，这里使用openssl内的EVP_SM3接口处理
		SM3_HASH(pData, Zlen + 4, cdgst, &hash_len);
		//最后一次计算，根据klen/32是否整除，若未整除，则截取最后一次hash的值
		if (i == index - 1)
		{
			if (Klen % 32 != 0)
			{
				nDgst = (Klen) % 32;
			}
		}
		memcpy(K + 32 * i, cdgst, nDgst);
		ct++;
	}
	return ;
}

